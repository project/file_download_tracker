<?php

namespace Drupal\file_download_tracker;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for file_download_entity.
 */
class FileDownloadEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
